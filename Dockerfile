FROM mcr.microsoft.com/dotnet/aspnet:7.0

WORKDIR /app
COPY publish/* .

EXPOSE 5037/tcp

ENV ASPNETCORE_HTTP_PORTS=5037
ENV ASPNETCORE_URLS=http://+:5037
ENV ASPNETCORE_ENVIRONMENT "$ENVIRONMENT"
CMD ["dotnet", "ServiceA.dll", "--launch-profile http"]